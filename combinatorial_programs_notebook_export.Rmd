---
output:
  html_notebook:
    toc: yes
    toc_float: yes
    code_folding: hide
    theme: journal
editor_options:
  chunk_output_type: inline
params: 
  set_title: "Placeholder Title"
  set_author: "Placeholder Author"
  set_theme: "Placeholder Theme"
title:  "Combinatorial Programming"
author: ""
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Input Data

```{r echo = FALSE, warning=FALSE, message=FALSE}
  df1 <- vrp_data()$clients
  df2 <- vrp_data()$depots
  df1$Group <- 'Visit' 
  df2$Group <- 'Vehicle'
  
  rbind(df1, df2) %>% 
    as.data.frame() %>% 
    mutate_if(is.numeric, round, 4) %>% 
    DT::datatable(class = 'cell-border stripe', 
                  options =list(initComplete = JS(
      "function(settings, json) {",
      "$(this.api().table().header()).css({'background-color': 
                               '#2a875d', 'color': '#fff'});",
      "}")))


```



# Combinatorial Program Solution
The optimal* solution of the vehicle routing problem is:
```{r echo=FALSE, warning=FALSE, message=FALSE}
 
 routes           <- vrp_solution_ga()$sol$routes %>% t() %>% as.data.frame() 
 rownames(routes) <- c('Vehicle', paste0('Visit ', 1:(nrow(routes) - 1)))
 colnames(routes) <- paste0('Route ', 1:ncol(routes))
 
 # Find the route that has the highest number of visits and drop all empty rows underneath.
 num_visits <- apply(routes, MARGIN = 2, FUN = function(x){sum(!is.na(x))})
 min_nas    <- max(num_visits)
 routes     <- routes[1:min_nas, ]
 
 routes %>% 
   DT::datatable(options = list(scroller = TRUE, 
                              scrollY = 450,
                              scrollX = 600), 
                 extensions = 'Scroller', 
                 caption = 'Optimal Routes',
                 style = 'bootstrap',
                 rownames = TRUE,
                 class = 'cell-border stripe')
 
```

# Solution Quality Analysis
```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=12, fig.height=7, fig.align='c'}
 routes <- vrp_solution_ga()$sol$routes
  paths  <- PlotPaths(routes = routes, locations = vrp_solution_ga()$clients, depots = vrp_solution_ga()$depots)
  
  paths_df <- dplyr::bind_rows(paths, .id = 'caretaker')
  colnames(paths_df) <- stringr::str_to_lower(colnames(paths_df))
  
  
  # Routes
g1 <-  ggplot(paths_df)+
          geom_path(aes(lon, lat, col = caretaker))+
          geom_point(aes(lon, lat), color = 'black', shape = 15, size = 5, data = vrp_solution_ga()$depots)+
          geom_point(aes(lon, lat, col = caretaker), size = 2.5)+
          theme_gray(base_size = 16)+
          theme(axis.ticks = element_line(size = 2, color = 'black'),
                text = element_text(family = input$figure_font_style), 
                legend.position = 'none')+
          ggtitle('Proposed Routes', subtitle = paste0('Longest Route: ', round(vrp_solution_ga()$sol$distance, 2), ' km'))+
          xlab('Longitude')+
          ylab('Latitude')
  
  
  # Optimization history
  historical_performance <- data.frame(Generation = 1:length(as.numeric(vrp_solution_ga()$sol$dist_history)),
                                       Fitness = as.numeric(vrp_solution_ga()$sol$dist_history))
  
  
g2 <-  ggplot(historical_performance, aes(x = Generation, y = Fitness))+
          geom_area(fill = 'darkblue', alpha = 0.85)+
          geom_point()+
          theme_gray(base_size = 16)+
          theme(text = element_text(family = input$figure_font_style))+
          ggtitle(label = 'Model Performance', subtitle = 'Optimization History')
        

gridExtra::grid.arrange(g1, g2, ncol = 2, widths = c(60, 40))
  
 

```
