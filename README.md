# RESCA

**R Environment for Scientific Computing and Analysis (RESCA)**


**Author**: Viktor Plamenov

**Keywords**: stochastic processes, optimization, machine learning, model fitting, probability, matrix algebra, calculus, finance
Shiny app: https://vik31415.shinyapps.io/resca/#
Repo: Viktor Plamenov / RESCA_ShinyCompetition2021 · GitLab
Demo at: https://www.youtube.com/playlist?list=PLsYsFdg-cbu3EW0vvyD0yQxrBByQpJNvC


**Abstract**: To a large extend the app was inspired by the vision behind Wolfram Alpha and Mathematica's functionality. My goal is to encapsulate as much computational power and functionality ranging from model fitting to combinatorial optimization.

**Full Description**: The R Environment for Scientific Computing and Analysis (RESCA) is an application which provides functionality in five broader categories.


**1. Estimation Engine**
The main goal of this module is to mimic a standard model fitting workflow. It starts with a data input section, where the user is expected to load a csv file and make sure it was read as expected. Then, the next two tabs return information for the missing values in each column and the summary statistics across all variables. The visualization section supports +20 different types of static/interactive figures and has plenty of customization options for each of them. Afterwards one could also inspect the distributional properties of each variable fitting a parametric/nonparametric distribution to the data. The last step has functionality for model fitting (regression, classification, time series, clustering). Once this is complete the user can export the results as an R
Notebook.

**2. Simulation Engine**
The simulation engine provides functionality in two main areas - probability distributions and stochastic processes.
2.1. Probability Distributions
The probability distributions simulation engine is designed to allow for the rapid number generation from a prespecified probability distribution. Currently over 40 discrete/continuous univariate distributions are supported. In terms of multivariate distributions, the application provides functionality for sampling from the multivariate normal, Dirichlet and student-t distributions.
2.2. Stochastic Processes
The stochastic processes simulation engine provides functionality for simulating discrete/continuous state and discrete/continuous time models. Currently the main focus is on the continuous state/time models aka stochastic differential equations, which are widely used in finance. Once the model is specified the user needs to select how many paths to be included in the simulation. For >2 paths the engine automatically creates figures showing the expected value, deviation, skewness and kurtosis of the process as a function of time.
2.3. Queueing Simulator
This application has a very specific use-case in mind. It solves queueing theory problems, where there are heterogeneous groups of arrival/service types.
2.4. Spatial Processes
This module allows for the the random number generation bounding the domain of the process within a country's borders. +160 country polygons are currently supported.


**3. Optimization Engine**
The optimization engine provides functionality for solving optimization problems separated into four categories. The linear optimization section relies on using the Simplex method and its various extension. The combinatorial optimizer is designed to solve mainly routing type of problems such as the travelling salesman or some of its more realistic extensions such as the vehicle routing problem (VRP).
3.1. Linear Programming
The main algorithm used here is the simplex along some of its extensions. The user can enter their problem and the application will automatically show how was the input interpreted. Once the linear program is solved, the solution could be exported as a standalone R Notebook.
3.2. Combinatorial Programming
3.3. Portfolio Optimization

**4. Toolboxes and Widgets**
This section provides several toolboxes for performing tasks ranging from computing your mortgage payments to estimating the sentiment score of all works available at the Gutenberg project.
A video list demoing parts of the application could be found at the link below.
