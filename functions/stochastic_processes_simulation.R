#======================================================================================================================#
#                                       Ornstein-uhlenbeck Process
#======================================================================================================================#

OrnsteinUhlenbeck <- function(t0 = 0, T, n, mu, kappa, sigma, X0){
  
  dw  <- rnorm(n, 0, sqrt(T/n))
  dt  <- (T - t0)/n
  x <- c(X0)
  
  for (i in 2:(n + 1)) {
    x[i]  <-  x[i - 1] + kappa*(mu - x[i - 1])*dt + sigma * dw[i-1]
  }
  
  return(x)
}


#======================================================================================================================#
#                                       Cox-Ingersoll-Ross Process
#======================================================================================================================#
CoxIngersoll <- function(t0 = 0, T, n, mu, kappa, sigma, X0){
  
  dw  <- rnorm(n, 0, sqrt(T/n))
  dt  <- (T - t0)/n
  x <- c(X0)
  
  for (i in 2:(n + 1)) {
    x[i]  <-  x[i - 1] + kappa*(mu - x[i - 1])*dt + sigma *(abs(x[i - 1])^0.5) * dw[i - 1]
  }
  
  return(x)
}


#======================================================================================================================#
#                                       Brownian Bridge Process
#======================================================================================================================#
BrownianBridge <- function(t0, t1, npoints, x_r, x_s, sigma){
  
  t <- seq(from = t0, to = t1, length.out = npoints)
  n <- npoints - 2
  X <- rep(0, npoints)
  X[1] <- x_r
  X[npoints] <- x_s
  s <- t[npoints]
  Z <- rnorm(n = npoints, mean = 0, sd = sigma)
  
  for (k in 1:npoints) {
    
    if(k == 1){
      X[k] <- x_r
      
    } else if(k == npoints){
      X[k] <- x_s
      
    } else if((k > 1) && (k < npoints)){
      
      mu  <- X[k - 1] + (x_s - X[k - 1])*(t[k] - t[k - 1]) / (s - t[k - 1])
      sig <- (s - t[k])*(t[k] - t[k - 1])/(s - t[k - 1])
      
      X[k] <- mu + sqrt(sig) * Z[k - 1]
    }
    
    
  }
  
  return(X)
  
}


#======================================================================================================================#
#                                   Constant Elasticity of Variance (CEV) Model                               
#======================================================================================================================#

CEVProcess <- function(t0, tn, npoints, mu, sigma, gamma, X0){
  
  dt <- (tn - t0)/npoints
  
  diffusionComponent <- sigma * sqrt(dt)
  X <- numeric(length = npoints)
  X[1] <- X0
  epsilon <- rnorm(n = npoints, mean = 0, sd = 1)
  
  for (i in 1:(npoints - 1)) {
    
    X[i + 1] <- X[i]*(1 + mu*dt) + diffusionComponent * epsilon[i] * abs(X[i])^gamma
    
  }
  
  return(X)
  
}


#======================================================================================================================#
#                                                CKLS Process                                 
#======================================================================================================================#
CKLSProcess <- function(t0 = 0, T, n, mu, kappa, sigma, gamma, X0){
  
  dw  <- rnorm(n, 0, sqrt(T/n))
  dt  <- (T - t0)/n
  x <- c(X0)
  
  for (i in 2:(n + 1)) {
    x[i]  <-  x[i - 1] + kappa*(mu - x[i - 1])*dt + sigma *(abs(x[i - 1])^gamma) * dw[i - 1]
  }
  
  return(x)
}


#======================================================================================================================#
#                                               Cauchy Process                                  
#======================================================================================================================#
CauchyProcess <- function(T = 10, n = 520){
  
  dt = T/n
  times <- seq(from = 0, to = T, by = 1)*dt
  Z <- rnorm(n + 1)/rnorm(n + 1)
  Z <- dt*Z 
  Z[1] <- 0
  X <- cumsum(Z)
  return(X)
}


#======================================================================================================================#
#                                       Variance-Gamma Process
#======================================================================================================================#
VarianceGammaProcess <- function(T, n, alpha, mu = 0, sigma = 1){
  
  dt <- T/n
  times <- seq(from = 0, to = T, length.out = n)
  X <- numeric(length = n)
  for (t in 2:n) {
    S <- rgamma(n = 1, shape = alpha * dt, scale = alpha)
    Z <- rnorm(1)
    X[t] = X[t - 1] + sigma*Z*sqrt(S) + mu*S
    
  }
  return(X)
  
}

#======================================================================================================================#
#                                           Wiener Process
#======================================================================================================================#
# Simulate a Wiener Process via its Quantile Function
WienerProcess <- function(T, n, mu, sigma, X0){
  
  p <- runif(n = n)
  tau <- seq(from = 0, to = T, length.out = n)
  sims <- numeric(n); sims[1] <- X0
  for (t in 2:n) {
    sims[t] <- tau[t]*mu - sqrt(2*tau[t])*sigma*erfc(2*p[t], inverse = TRUE)
  }
  
  return(sims)
}


#======================================================================================================================#
#                                           GBM Process 
#======================================================================================================================#
GeometricBrownianProcess <- function(tn, t0, n, mu, sigma, X0){
  
  dt <- (tn - t0)/n
  
  diffusionComponent <- sigma * sqrt(dt)
  X <- c()
  X[1] <- X0
  epsilon <- rnorm(n = n, mean = 0, sd = 1)
  
  for (i in 1:n) {
    
    X[i + 1] <- X[i]*(1 + mu*dt) + diffusionComponent * rnorm(n = 1, mean = 0, sd = 1) * X[i]
    
  }
  
  return(X)
  
}