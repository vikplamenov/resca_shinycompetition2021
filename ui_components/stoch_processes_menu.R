

stoch_processes_menu <- function(){
  bs4TabItem(tabName = 'stoch_process', 
          
          tabBox(id = 'stoch_process',
                 width = 12,
                 height = 1200,
                 collapsible = FALSE, 
                 maximizable = TRUE,
                 
                 tabPanel(title = 'Stochastic Processes',
                          fluidRow(
                          sidebarPanel(width = 3, 
                                       
                                       # Help Menu for the Simulation Engine
                                       introjsUI(),
                                       actionButton(icon = icon('question-circle', 
                                                                lib = 'font-awesome'),
                                                    inputId = "sim_stoch_proc_menu", 
                                                    label = "Help",
                                                    class = "btn btn-info pull-right",
                                                    style="color: #fff; background-color: #337ab7; 
                                                      border-color: #2e6da4;
                                                      border-width: 2px"
                                       ),
                                       
                                       div(id = 'stoch_proc_time_type',
                                           selectInput(inputId = 'stoch_proc_class1',
                                                       label = 'Process Time', 
                                                       choices = c('Discrete', 'Continuous'), 
                                                       selected = 'Continuous')),
                                       div(id = 'stoch_proc_state_type',
                                           selectInput(inputId = 'stoch_proc_class2', 
                                                       label = 'Process State',
                                                       choices = c('Discrete', 'Continuous'), 
                                                       selected = 'Continuous')),
                                       shinyWidgets::actionBttn(
                                         inputId = "generate_stoch_processes",
                                         label = 'Generate Simulations',
                                         style = "unite",
                                         block = TRUE,
                                         color = "success"
                                       ),
                                       br(),
                                       conditionalPanel(condition = "input.stoch_proc_class1 == 'Discrete'&
                                             input.stoch_proc_class2 == 'Discrete'",
                                                        selectInput(inputId = 'stoch_proc_disc_disc', 
                                                                    label = 'Process', 
                                                                    choices = c('Random Walk', 'Discrete Markov Process'), 
                                                                    selected = 'Discrete Markov Process')   
                                       ),
                                       
                                       conditionalPanel(condition = "input.stoch_proc_class1 == 'Discrete'&
                                             input.stoch_proc_class2 == 'Continuous'",
                                                        
                                                        selectInput(inputId = 'stoch_proc_disc_cont', 
                                                                    label = 'Process',
                                                                    choices = c('AR', 'MA', 'ARMA', 'GARCH'), 
                                                                    selected = 'AR'),
                                                        
                                                        
                                                        conditionalPanel(condition = "input.stoch_proc_disc_cont == 'ARMA'", 
                                                                         
                                                                         splitLayout(cellWidths = c('50%', '50%'),
                                                                                     
                                                                                     numericInput(inputId = "sim_arma_ar", 
                                                                                                  label = 'AR(1)', 
                                                                                                  value = 0.85, 
                                                                                                  min = -0.999999, 
                                                                                                  max = 0.999999),
                                                                                     numericInput(inputId = "sim_arma_ma", 
                                                                                                  label = "MA(1)", 
                                                                                                  value = 0.8, 
                                                                                                  min = -0.999999, 
                                                                                                  max = 0.999999)
                                                                                     
                                                                                     
                                                                         )
                                                        ),
                                                        
                                                        conditionalPanel(condition = "input.stoch_proc_disc_cont == 'MA'",
                                                                         numericInput(inputId = "sim_ma", 
                                                                                      label = "MA(1) Parameter",
                                                                                      value = 0.8, 
                                                                                      min = -0.999999,
                                                                                      max = 0.999999)
                                                        ),
                                                        
                                                        conditionalPanel(condition = "input.stoch_proc_disc_cont == 'AR'",
                                                                         numericInput(inputId = "sim_ar",
                                                                                      label = 'AR(1) Parameter', 
                                                                                      value = 0.85,
                                                                                      min = -0.999999,
                                                                                      max = 0.999999)),
                                                        
                                                        conditionalPanel(condition = "input.stoch_proc_disc_cont == 'GARCH'",
                                                                         selectInput(inputId = 'garchModel', 
                                                                                     label = 'Type', 
                                                                                     choices = c('Garch')), # add more types later on
                                                                         
                                                                         conditionalPanel(condition = "input.garchModel == 'Garch'", 
                                                                                          numericInput(inputId = 'garchOmega', 
                                                                                                       label = 'Omega', 
                                                                                                       value = 0.10),
                                                                                          
                                                                                          splitLayout(cellWidths = c('50%', '50%'),
                                                                                                      numericInput(inputId = 'garchAlpha', label = 'Alpha', 
                                                                                                                   value = 0.09),
                                                                                                      numericInput(inputId = 'garchBeta', label = 'Beta', 
                                                                                                                   value = 0.75)
                                                                                          )
                                                                                          
                                                                         ),
                                                                         selectInput(inputId = 'garchDistribution', 
                                                                                     label = 'Innovations',
                                                                                     choices = c('Normal', 
                                                                                                 'Skew Normal', 
                                                                                                 'Student',
                                                                                                 'Skew Student', 
                                                                                                 'Generalized Error Distribution'),
                                                                                     selected = 'Normal')
                                                        ),
                                                        
                                                        conditionalPanel(condition = "input.stoch_proc_disc_cont == 'AR' || input.stoch_proc_disc_cont == 'MA'",
                                                                         checkboxInput(inputId = "ts_custom_innov", 
                                                                                       label = "Custom Innovations", 
                                                                                       value = FALSE)),
                                                        
                                                        conditionalPanel(condition = "input.ts_custom_innov == true",
                                                                         selectInput(inputId = "custom_innov_dist", 
                                                                                     label = "Distribution", 
                                                                                     choices = c("Normal", 'Student(Inactive)', 
                                                                                                 "Stable (Inactive)"), 
                                                                                     selected = "Normal"),
                                                                         
                                                                         conditionalPanel(condition = "input.custom_innov_dist == 'Normal'",
                                                                                          
                                                                                          splitLayout(cellWidths = c("50%", "50%"),
                                                                                                      numericInput("ts_innov_mean",  
                                                                                                                   label = "Mean", 
                                                                                                                   value = 0),
                                                                                                      numericInput("ts_innov_sigma", 
                                                                                                                   label = "Std",  
                                                                                                                   value = 1)
                                                                                          )
                                                                         )
                                                                         
                                                        )
                                                        
                                       ),
                                       
                                       # conditionalPanel(condition = "input.stoch_proc_class1 == 'Continuous'&
                                       #     input.stoch_proc_class2 == 'Discrete'",
                                       #                  selectInput(inputId = 'stoch_proc_cont_disc', label = 'Process',
                                       #                              choices = c('Poisson', 'Queueing', 'Telegraph'))),
                                       
                                       conditionalPanel(condition = "input.stoch_proc_class1 == 'Continuous'&
                                                          input.stoch_proc_class2 == 'Continuous'",
                                                        selectInput(inputId = 'stoch_proc_cont_cont', 
                                                                    label = 'Process',
                                                                    choices = c('Wiener', 
                                                                                'GBM', 
                                                                                'Ornstein-Uhlenbeck', 
                                                                                "Brownian Bridge", 
                                                                                "CEV",
                                                                                'Cox-Ingersoll-Ross', 
                                                                                "CKLSProcess",
                                                                                'Cauchy', 
                                                                                'Variance-Gamma (Inactive)'), 
                                                                    selected = 'Ornstein-Uhlenbeck'),
                                                   
                                                        splitLayout(cellWidths = c("50%", "50%"),
                                                                    
                                                                    numericInput(inputId = 'time_start', 
                                                                                 label = 'Start Time', 
                                                                                 value = 0, 
                                                                                 min = 0),
                                                                    numericInput(inputId = 'time_horizon', 
                                                                                 label = 'End Time', 
                                                                                 value = 10, 
                                                                                 min = 0.1)),
                                                        splitLayout(cellWidths = c("50%", "50%"),
                                                                    numericInput(inputId = 'stoch_pr_bootstrap_cont',
                                                                                 label = '# Paths', 
                                                                                 value = 21,
                                                                                 min = 1, 
                                                                                 max = 300,
                                                                                 step = 10),
                                                                    numericInput(inputId = 'data_size', 
                                                                                 label = '# Points', 
                                                                                 value = 120)),
                                                        
                                                        conditionalPanel("input.stoch_proc_cont_cont == 'Wiener'", 
                                                                         splitLayout(cellWidths = c("50%", "50%"),
                                                                                     numericInput(inputId = 'wiener_drift',
                                                                                                  label = 'Drift', 
                                                                                                  value = 0),
                                                                                     numericInput(inputId = 'wiener_sigma', 
                                                                                                  label = 'Sigma', value = 1, 
                                                                                                  min = 0))   
                                                        ),
                                                        conditionalPanel("input.stoch_proc_cont_cont == 'GBM'", 
                                                                         splitLayout(cellWidths = c("50%", "50%"),
                                                                                     numericInput(inputId = 'gbm_drift',
                                                                                                  label = 'Mu', 
                                                                                                  value = 0.05),
                                                                                     numericInput(inputId = 'gbm_sigma', 
                                                                                                  label = 'Sigma', 
                                                                                                  value = 0.04, 
                                                                                                  min = 0)),
                                                                         numericInput(inputId = 'gbm_x0', 
                                                                                      label = 'Initial Condition', 
                                                                                      value = 10,
                                                                                      min = 0)
                                                        ),
                                                        conditionalPanel(condition = "input.stoch_proc_cont_cont == 'Ornstein-Uhlenbeck'||
                                                              input.stoch_proc_cont_cont == 'Cox-Ingersoll-Ross' ||
                                                              input.stoch_proc_cont_cont == 'CKLSProcess'",
                                                                         splitLayout(cellWidths = c("50%", "50%"),
                                                                                     numericInput(inputId = 'ou_mean', 
                                                                                                  label = 'Mean', 
                                                                                                  value = 10),
                                                                                     numericInput(inputId = "ou_sigma", 
                                                                                                  label = 'Volatility', 
                                                                                                  value = 0.65, 
                                                                                                  min = 0.00001)),
                                                                         numericInput(inputId = 'ou_kappa', 
                                                                                      label = "Mean-Reversion", 
                                                                                      value = 0.5, 
                                                                                      min = 0),
                                                                         numericInput(inputId = 'ou_initial_state', 
                                                                                      label = 'Initial State', 
                                                                                      value = 5)
                                                        ),
                                                        conditionalPanel(condition = "input.stoch_proc_cont_cont == 'CKLSProcess' ||
                                                                           input.stoch_proc_cont_cont == 'CEV'",
                                                                         numericInput(inputId = 'cev_gamma', 
                                                                                      label = "Elasticity",
                                                                                      value = 0.4)),
                                                        
                                                        conditionalPanel(condition = "input.stoch_proc_cont_cont == 'CEV'",
                                                                         splitLayout(cellWidths = c("50%", "50%"),
                                                                                     numericInput(inputId = 'cev_drift', 
                                                                                                  label = "Drift", 
                                                                                                  value = 0.05),
                                                                                     numericInput(inputId = 'cev_sigma', 
                                                                                                  label = "Sigma", 
                                                                                                  value = 0.4)),
                                                                         #numericInput(inputId = 'cev_gamma', label = "Elasticity", value = 0.4),
                                                                         numericInput(inputId = 'cev_X0', 
                                                                                      label = "Initial Condition", 
                                                                                      value = 10)
                                                                         
                                                        ),
                                                        
                                                        conditionalPanel(condition = "input.stoch_proc_cont_cont == 'Brownian Bridge'",
                                                                         splitLayout(cellWidths = c("50%", "50%"),
                                                                                     numericInput(inputId = "bb_X_start", 
                                                                                                  label = "Initial Value", 
                                                                                                  value = 10),
                                                                                     numericInput(inputId = "bb_X_end", 
                                                                                                  label = "End Value", 
                                                                                                  value = 12)),
                                                                         numericInput(inputId = "bb_sigma", 
                                                                                      label = "Process Volatility", 
                                                                                      value = 0.65,
                                                                                      min = 0.00001)
                                                                         
                                                        )
                                                        
                                                        
                                       ),
                                       
                                       conditionalPanel(condition = "input.stoch_proc_class1 == 'Discrete'",
                                                        splitLayout(cellWidths = c("50%", "50%"),
                                                                    numericInput(inputId = 'stoch_pr_bootstrap', 
                                                                                 label = '# Paths', 
                                                                                 value = 1, 
                                                                                 min = 1, 
                                                                                 max = 500, 
                                                                                 step = 5),
                                                                    numericInput(inputId = 'sim_size', 
                                                                                 label = 'Sample Size', 
                                                                                 value = 100, 
                                                                                 min = 1)
                                                        )
                                       )
                                       
                          ),
                          
                          box(
                            title = 'Simulations', 
                            solidHeader = TRUE,
                            width = 9, 
                            height = 480,
                            closable = FALSE, 
                            maximizable = TRUE,
                            status = "warning", 
                            collapsible = TRUE,
                            dropdownMenu = boxDropdown(icon = icon('save'),
                                                       shinyWidgets::downloadBttn(
                                                         outputId =  "download_stoch_process",
                                                         label = 'Download Simulations',
                                                         style = "unite",
                                                         block = TRUE,
                                                         color = "success",
                                                         size = 'xs'
                                                       )
                            ),
                            sidebar = boxSidebar(
                              id = "stoch_proc_properties_sidebar",
                              conditionalPanel(condition = "input.stoch_proc_class1 == 'Continuous'&
                                                          input.stoch_proc_class2 == 'Continuous'",
                                               uiOutput(outputId = "stoch_pdes_latex"))
                            ),
                            
                            br(),
                            box(withSpinner(plotOutput(outputId = 'stoch_proc_plot'), type = 8),
                                plotOutput(outputId = 'stoch_proc_std_dynamics', 
                                           height = 480), 
                                title = 'Process Graph', 
                                status = 'primary', 
                                solidHeader = TRUE, 
                                width = 12, 
                                maximizable = TRUE, 
                                collapsed = TRUE, 
                                collapsible = TRUE,
                                sidebar = boxSidebar(
                                  id = "stoch_multiple_proc_properties_sidebar",
                                  conditionalPanel(condition = "input.stoch_pr_bootstrap_cont > 1",
                                                       conditionalPanel(condition = "input.stoch_pr_bootstrap_cont > 5",
                                                                        numericInput(inputId = 'slice_dist_time', 
                                                                                     label = 'Slice Process at t = ', 
                                                                                     value = 1,
                                                                                     min = 0.001),
                                                                        numericInput(inputId = "stoch_proc_ci",
                                                                                     label = 'Confidence Interval', 
                                                                                     value = 0.05, 
                                                                                     min = 0.001, 
                                                                                     max = 0.999))
                                                  
                                  )
                                )
                                ),  
                            uiOutput(outputId = 'stoch_table_placeholder_logo'),
                            withSpinner(DT::dataTableOutput(outputId = 'stoch_proc_dt'), type = 8)
                          )
                          
                          # box(title = 'Simulations', 
                          #     status = 'primary', 
                          #     solidHeader = TRUE,
                          #     width = 9,
                          #   box(withSpinner(plotOutput(outputId = 'stoch_proc_plot')),
                          #                   plotOutput(outputId = 'stoch_proc_std_dynamics', 
                          #                                height = 480), 
                          #       title = 'Process Graph', 
                          #       status = 'primary', 
                          #       solidHeader = TRUE, 
                          #       width = 19, 
                          #       collapsed = TRUE, 
                          #       collapsible = TRUE),
                          #   conditionalPanel(condition = "input.stoch_proc_class1 == 'Continuous'&
                          #                               input.stoch_proc_class2 == 'Continuous'",
                          #                    uiOutput(outputId = "stoch_pdes_latex")),
                          #   uiOutput(outputId = 'stoch_table_placeholder_logo'),
                          #   withSpinner(DT::dataTableOutput(outputId = 'stoch_proc_dt'))
                          #   )
                          
                 )
                 )
          )
  )
}