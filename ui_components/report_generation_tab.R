
report_generation_tab <- function(){
  tabPanel(title = 'Report Generation',
           icon = icon('folder-plus',
                       lib = 'font-awesome'), 
           sidebarPanel(width = 4,
                        textInput(inputId = 'notebook_title', 
                                  label = 'Notebook Title', 
                                  value = 'Title'),
                        box(
                          width = 12, 
                          title = 'Document Settings', 
                          status = 'primary',
                          solidHeader = TRUE, 
                          collapsible = TRUE,
                          collapsed = FALSE, 
                          textInput(inputId = 'notebook_author', 
                                    label = 'Author', 
                                    value = 'Author Name'),
                          selectInput(inputId = 'notebook_theme', 
                                      label = 'Notebook Theme', 
                                      choices = c('journal', 
                                                  'lumen', 
                                                  'cerulean',
                                                  'cosmo', 
                                                  'paper', 
                                                  'sandstone'), 
                                      selected = 'journal')
                          
                        ),
                        
                        radioButtons('format', 'Document format', 
                                     c('HTML', 'PPT'),
                                     inline = TRUE),
                        downloadButton('downloadReport', 
                                       label = 'Generate and Download Report')),
           h5("This section is in active development. The end goal is to provide a framework for automatic 
                            report generation in HTML, LaTeX, PDF, Word, and Power Point.")
  )
}