


matrix_algebra <- function(){
  tabItem(tabName = 'matrix_algebra_calculator',
          tabPanel(title = 'Matrix Algebra',
                   fluidRow(
                   sidebarPanel(width = 4, 
                                shinyWidgets::pickerInput(inputId = 'matrix_operation', 
                                                          label = 'Matrix Operation', 
                                                          choices = list(
                                                            `Single Matrix` = c('Trace',
                                                                                'Transpose',
                                                                                'Determinant',
                                                                                'Power',
                                                                                'Inverse',
                                                                                'Rank',
                                                                                'Minors',
                                                                                'Eigenvalues',
                                                                                'Eigenvectors',
                                                                                'Adjoint',
                                                                                'SVD', 
                                                                                'Cholesky'),
                                                            `Two Matrices` = c('Add',
                                                                               'Subtract',
                                                                               'Multiply')),
                                                          selected = 'Power', 
                                                          multiple = FALSE,
                                                          options = list(
                                                            `actions-box` = TRUE,
                                                            style = "btn-primary",
                                                            size = 10
                                                          )),
                                br(),
                                conditionalPanel(condition = "input.matrix_operation == 'Minors'", 
                                                 
                                                 splitLayout(cellWidths = c('50%','50%'),
                                                             numericInput(inputId = 'minor_i', label = 'i', value = 1),
                                                             numericInput(inputId = 'minor_j', label = 'j', value = 1)
                                                             
                                                 )
                                ),
                                
                                box(title = 'Matrix A',
                                    status = 'primary', 
                                    solidHeader = TRUE,
                                    maximizable = TRUE,
                                    collapsible = TRUE,
                                    collapsed = FALSE, 
                                    width = 12,
                                    
                                    splitLayout(cellWidths = c("50%", "50%"),
                                                numericInput(inputId = 'matrix_a_cols', 
                                                             label = '# Columns', 
                                                             min = 1, 
                                                             value = 4, 
                                                             max = 18, 
                                                             step = 1),
                                                numericInput(inputId = 'matrix_a_rows', 
                                                             label = '# Rows', 
                                                             min = 0, 
                                                             value = 4, 
                                                             max = 100, 
                                                             step = 1)
                                                ),
                                        uiOutput(outputId = 'MatrixInputUI_A')
                                        
                                ),
                                
                                conditionalPanel(condition = "input.matrix_operation == 'Power'",
                                                 numericInput(inputId = 'matrix_power_calc', 
                                                              label = 'Matrix Power', 
                                                              value = 3, 
                                                              min = 1, 
                                                              step = 1)
                                                 
                                ),
                                conditionalPanel(condition = "input.matrix_operation == 'Add'|
                                                        input.matrix_operation == 'Subtract'|
                                                        input.matrix_operation == 'Multiply'",
                                                 box(title = 'Matrix B', 
                                                     status = 'primary', 
                                                     solidHeader = TRUE,
                                                     maximizable = TRUE,
                                                     collapsible = TRUE,
                                                     collapsed = FALSE, 
                                                     width = 12,
                                                     splitLayout(cellWidths = c("50%", "50%"),
                                                                 numericInput(inputId = 'matrix_b_ncols', 
                                                                              label = '# Columns', 
                                                                              min = 1,
                                                                              value = 2, 
                                                                              max = 15,
                                                                              step = 1),
                                                                 numericInput(inputId = 'matrix_b_nrows', 
                                                                              label = '# Rows', 
                                                                              min = 0,
                                                                              value = 4, 
                                                                              max = 100, 
                                                                              step = 1)),
                                                     uiOutput(outputId = 'MatrixInputUI_B')
                                                 ))
                                
                                
                   ),
                   box(solidHeader = TRUE, 
                       status = 'primary', 
                       width = 8,
                       maximizable = TRUE, 
                       collapsible = FALSE,
                       shinyBS::bsAlert("alert"),
                       dropdownMenu = boxDropdown(icon = icon('save'),
                         shinyWidgets::downloadBttn(
                           outputId =  "download_matrix_eqns",
                           label = 'Download Results',
                           style = "unite",
                           block = TRUE,
                           color = "success",
                           size = 'xs'
                         )
                       ),
                       h5('The input matrix is given by: '),
                       div(style = 'overflow-x: scroll',
                        uiOutput(outputId = 'matrix_a_input')
                       ),
                       conditionalPanel(
                         condition = "input.matrix_operation == 'Add'|
                                input.matrix_operation == 'Subtract'|
                                input.matrix_operation == 'Multiply'",
                         uiOutput(outputId = 'matrix_b_input')),
                       div(style = 'overflow-x: scroll',
                           uiOutput(outputId = 'output_matrix'))
                       
                   )
                   )
                   )
  )
}



linear_systems <- function(){
  tabItem(tabName = 'linear_systems_calculator',
          fluidRow(
          sidebarPanel(width = 4,
                       sliderInput(inputId = 'number_variables', 
                                   label = 'Number of variables', 
                                   value = 4, 
                                   min = 2,
                                   max = 10),
                       br(),
                       uiOutput(outputId = 'MatrixInput_Eqns'),
                       actionButton(inputId = 'solve_linear_system', 
                                    label = 'Solve System',
                                    style = "color: #fff; 
                                           background-color: #982b2b; 
                                           border-color: #000000;
                                           border-width: 2px"),
          ),
          box(width = 8, 
              solidHeader = TRUE, 
              status = 'primary', 
              collapsible = FALSE,
              maximizable = TRUE,
              dropdownMenu = boxDropdown(icon = icon('save'),
                                         shinyWidgets::downloadBttn(
                                           outputId =  "download_linear_systems",
                                           label = 'Download Results',
                                           style = "unite",
                                           block = TRUE,
                                           color = "success",
                                           size = 'xs'
                                         )
              ),
              h5('The matrix representation of the linear system is: '),
              shinycssloaders::withSpinner(
                uiOutput(outputId = 'linear_system_form'), type = 8
              ),
              h5('The classical representation of the linear system is: '),
                uiOutput(outputId = 'linear_system_classical_form'),
              h5('The solution of the linear system is obtained at: '),
                uiOutput(outputId = 'linear_system_solution')
          )
          )
  )
}



quadratic_eqns <- function(){
  tabItem(tabName = 'quadratic_eqns_calculator',
          fluidRow(
            
            sidebarPanel(width = 3,
                         h5('Enter the coefficients of the quadratic equation '),
                         uiOutput(outputId = 'quadratic_text'),
                         splitLayout(cellWidths = c('33%', '33%', '33%'),
                                     textInput(inputId = 'quad_coef', 
                                               label = 'a', 
                                               value = -3),
                                     textInput(inputId = 'linear_coef', 
                                               label = 'b', 
                                               value = -2),
                                     textInput(inputId = 'zeroth_coef', 
                                               label = 'c', 
                                               value = 5))
                         
                         
            ),
            
            box(width = 9, 
                collapsible = FALSE,
                div(style = 'overflow-x: scroll',
                    box(width = 12,            
                        solidHeader = TRUE,
                        status = 'primary', 
                        collapsible = FALSE,
                        h5('The entered equation was interpreted as:'),
                          uiOutput(outputId = 'quadratic_eqn_form'),
                        h5('and has a solution set given by:'),
                          uiOutput(outputId = 'quadratic_eqn_solution'),
                        h5('The discriminant of the equation is given by:'),
                        uiOutput(outputId = 'equation_discriminant')
                    )),
                column(
                  width = 7,
                  align = 'center',
                  div(style = 'overflow-x: scroll',
                      plotOutput(outputId = 'quadratic_eqn_figure'))
                  
                )
            )
            
          )
          )
}

calculus_tab <- function(){
  tabItem(tabName = 'calculus_calculator',
          fluidRow(
            sidebarPanel(width = 4, 
                         selectInput(inputId = 'operation_calculus', 
                                     label = 'Operation',
                                     choices = c('Differentiation', 
                                                 'Integration'), 
                                     selected = 'Differentiation'),
                         
                         conditionalPanel(condition = "input.operation_calculus == 'Differentiation'",
                                          numericInput(inputId = 'derivative_order', 
                                                      label = 'Derivative Order', 
                                                      min = 1,  
                                                      value = 1, 
                                                      step = 1)),
                         conditionalPanel(condition = "input.operation_calculus == 'Integration'",
                                          checkboxInput(inputId = 'definite_integration', 
                                                        label = 'Definite Integral', 
                                                        value = FALSE),
                                          
                                          conditionalPanel(condition = "input.definite_integration == true",
                                                           splitLayout(cellWidths = c('50%', '50%'),
                                                                       
                                                                       textInput(inputId = 'integration_a',
                                                                                 label = 'Lower Boundary', 
                                                                                 value = '0'),
                                                                       textInput(inputId = 'integration_b',
                                                                                 label = 'Upper Boundary', 
                                                                                 value = 'pi/2')
                                                                       
                                                           )
                                          )
                                          
                         ),
                         shinyAce::aceEditor(outputId = 'ace_editor_function_input', 
                                             value = 'exp(-x^2) + cos(x) + Rational(5,8)', 
                                             height = '120px', 
                                             theme = "cobalt", 
                                             mode = 'r', 
                                             fontSize = 15, 
                                             highlightActiveLine = TRUE))
            ,
            div(style = 'overflow-x: scroll',
                shinycssloaders::withSpinner(uiOutput(outputId = 'function_latex'), type = 8)
                )
          )
          
          
  )
          
  
}

#======================================================================================================================#
#                 Extract the unit conversion names and relabel them as per client request.
#======================================================================================================================#
units_df <- measurements:::.conversions
require(measurements)
# Format the names as per client request
new_units <- strsplit(units_df$unit, split = "(?<=[a-zA-Z])\\s*(?=[0-9])", perl = TRUE)

new_names <- sapply(1:length(new_units), function(x){
  if(length(new_units[[x]]) > 1){
    paste0(new_units[[x]][1], '^', new_units[[x]][2])  
  } else{
    paste0(new_units[[x]][1])
  }
  
  
})


unit_conversion <- function(){

  tabItem(tabName = 'unit_conversions',
          tabPanel(title = "Unit Conversions",
                   width = 12, 
                   height = 750,
                   fluidRow(
                     sidebarPanel(width = 3,
                                  numericInput(inputId = 'metric_value', 
                                               label = 'Units', 
                                               value = 10),
                                  selectInput(inputId = 'conv_type', 
                                              label = 'Unit Type', 
                                              choices = c('Length',
                                                          'Weight',
                                                          'Temperature',
                                                          'Volume', 
                                                          'Angle', 
                                                          'Speed',
                                                          'Energy'), 
                                              selected = 'Angle'),
                                  
                                  conditionalPanel(condition = "input.conv_type == 'Length'",
                                                   selectInput(inputId = 'from_length', 
                                                               label = 'From Unit', 
                                                               choices  = units_df$new_unit_name[units_df$dim == 'length'],
                                                               selected = units_df$new_unit_name[units_df$dim == 'length'][1]),
                                                   
                                                   selectInput(inputId = 'to_length', 
                                                               label = 'To Unit', 
                                                               choices  = units_df$new_unit_name[units_df$dim == 'length'],
                                                               selected = units_df$new_unit_name[units_df$dim == 'length'][2])
                                  ),
                                  
                                  conditionalPanel(condition = "input.conv_type == 'Weight'",
                                                   selectInput(inputId = 'from_weigth', 
                                                               label = 'From Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'mass'], 
                                                               selected = units_df$unit[units_df$dim == 'mass'][1]),
                                                   
                                                   selectInput(inputId = 'to_weigth', 
                                                               label = 'To Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'mass'],
                                                               selected = units_df$unit[units_df$dim == 'mass'][2])
                                                   
                                  ),
                                  
                                  conditionalPanel(condition = "input.conv_type == 'Temperature'", 
                                                   selectInput(inputId = 'from_temp',
                                                               label = 'From Unit', 
                                                               choices  =  units_df$unit[units_df$dim == 'temperature'], 
                                                               selected =  units_df$unit[units_df$dim == 'temperature'][1]
                                                   ),
                                                   selectInput(inputId = 'to_temp',
                                                               label = 'To Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'temperature'], 
                                                               selected = units_df$unit[units_df$dim == 'temperature'][2]
                                                   )
                                  ),
                                  
                                  conditionalPanel(condition = "input.conv_type == 'Volume'", 
                                                   selectInput(inputId = 'from_volume',
                                                               label = 'From Unit', 
                                                               choices  =  units_df$new_unit_name[units_df$dim == 'volume'], 
                                                               selected =  units_df$new_unit_name[units_df$dim == 'volume'][1]
                                                   ),
                                                   selectInput(inputId = 'to_volume',
                                                               label = 'To Unit', 
                                                               choices  = units_df$new_unit_name[units_df$dim == 'volume'], 
                                                               selected = units_df$new_unit_name[units_df$dim == 'volume'][2]
                                                   )
                                  ),
                                  
                                  conditionalPanel(condition = "input.conv_type == 'Angle'", 
                                                   selectInput(inputId = 'from_angle',
                                                               label = 'From Unit', 
                                                               choices  =  units_df$unit[units_df$dim == 'angle'], 
                                                               selected =  units_df$unit[units_df$dim == 'angle'][1]
                                                   ),
                                                   selectInput(inputId = 'to_angle',
                                                               label = 'To Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'angle'], 
                                                               selected = units_df$unit[units_df$dim == 'angle'][2]
                                                   )
                                  ),
                                  
                                  conditionalPanel(condition = "input.conv_type == 'Speed'", 
                                                   selectInput(inputId = 'from_speed',
                                                               label = 'From Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'speed'], 
                                                               selected = units_df$unit[units_df$dim == 'speed'][1]
                                                   ),
                                                   selectInput(inputId = 'to_speed',
                                                               label = 'To Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'speed'], 
                                                               selected = units_df$unit[units_df$dim == 'speed'][2]
                                                   )
                                  ),
                                  
                                  conditionalPanel(condition = "input.conv_type == 'Energy'", 
                                                   selectInput(inputId = 'from_energy',
                                                               label = 'From Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'energy'], 
                                                               selected = units_df$unit[units_df$dim == 'energy'][1]
                                                   ),
                                                   selectInput(inputId = 'to_energy',
                                                               label = 'To Unit', 
                                                               choices  = units_df$unit[units_df$dim == 'energy'], 
                                                               selected = units_df$unit[units_df$dim == 'energy'][2]
                                                   )
                                  )
                                  
                                  
                                  
                     ),
                     box(width = 8, 
                         solidHeader = TRUE,
                         status = 'primary', 
                         uiOutput(outputId = 'unit_conversion'))
                   )
          )
  )
  
  
}