## 1. Type of operation

-   Differentiation
-   Integration
    -   Indefinite
    -   Definite

## 2. Input Format

-   Arithmetic Operations
    -   Addition: “+”, e.g. 5+x
    -   Subtraction: “-”, e.g. 2-x
    -   Multiplication: ’*’, e.g. 2*x
    -   Division: “/”, e.g. 1/x
    -   Numeric Fractions: Rational(a,b) = a/b
-   Functions
    -   Every function should be written in the form $f(x)$,
        e.g. $cos(x)$, $ln(x)$, $\exp(x)$. Other types of
        brackets **will not work**. Keep in mind the calculator is set
        up to recognize only *x* as a variable. Therefore, it does not
        handle other symbolic parameters within the function, and will
        not compute any values if you provide it with other symbolic
        input.
-   Powers
    -   Raising a number to a power could be achieved via ‘^’, e.g. x^2
        is interpreted as *x*<sup>2</sup>, and “sqrt(x)” as $\\sqrt{x}$.
-   Trigonometric Functions
    -   Trigonometric - sin(x), cos(x), tan(x), cot(x), sec(x), csc(x)
    -   Hyperbolic Trigonometric - sinh(x), cosh(x), tanh(x), coth(x),
        sech(x), csch(x)
    -   Inverse Trigonometric - asin(x), acos(x), atan(x), acot(x),
        asec(x), acsc(x))
-   Exponential and Logs - $e$ = exp (1), Natural Log = log(x, base = exp(1))

## 3. Output Formats 
 By default the calculator tries to solve the
    problem analytically and obtain an exact result. Whenever an
    integral could not be solved in closed form the calculator will
    simply return the original integral.
